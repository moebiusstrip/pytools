import atexit
import time
import traceback
import math


class Timeit:
    """Use to time a single function, will print the result on exit.
        Options: prefix=<string>  'id to prefix for this printout'
                 printtb=<bool>   'if true prints a stacktrace of the function call with the highest runtime'
                 printhist=<bool> 'if true prints a histogram with 10bins to show distribution of average call time' 
    """
    def __init__(self, printtb=True, prefix="@timeit", printhist=False):
        self.lowtime = 0.
        self.hightime = 0.
        self.timesum = 0.
        self.callcount = 0
        self.hightimeargs = []
        self.hightimekwargs = {}
        self.lowtimeargs=[]
        self.lowtimekwargs = {}
        self.hightimetb = None
        self.histtime = []
        self.printhist = printhist
        self.printtb = printtb
        self.prefix = prefix
        def printout_wrapper():
            self.printout()
            if self.printhist:
                self.printhistogram()
        atexit.register(printout_wrapper)
    def printout(self):
        msg = []
        msg.append(">> %s"%(self.f.__name__))
        if self.f.__doc__:
            msg.append("   %s"%(self.f.__doc__))
        if self.callcount > 0:
            msg.append("   Total: %f ms on %d calls."%(self.timesum*1000.,self.callcount))
            kit = ""
            if len( self.lowtimeargs) > 0 and len(self.lowtimekwargs)>0: 
                kit = ", "
            msg.append("     L: %f ms CALL %s(%s%s%s)"%( self.lowtime*1000.,
                            self.f.__name__,",".join([str(i) for i in self.lowtimeargs]),
                            kit, 
                            ",".join(["%s=%s"%(str(k),str(v)) for k,v in self.lowtimekwargs.items()])
                            ))
            msg.append("     A: %f ms"%( self.timesum/self.callcount*1000.))
            kit = ""
            if len( self.hightimeargs) > 0 and len(self.hightimekwargs)>0: 
                kit = ", "
            msg.append("     H: %f ms CALL %s(%s%s%s)"%( self.hightime*1000.,
                            self.f.__name__,",".join([str(i) for i in self.hightimeargs]),
                            kit, 
                            ",".join(["%s=%s"%(str(k),str(v)) for k,v in self.hightimekwargs.items()])
                ))
            if self.printtb:
                lastnum = 0
                if self.hightimetb:
                    tb = self.hightimetb[0]
                    msg.append("       Trace starts at %s:%d (%s)"%(tb[0],tb[1],tb[2]))
                    lastnum = tb[1]
                for tb in self.hightimetb:
                    msg.append("         %s %s:%d (%s)"%(tb[3].ljust(40),tb[0],tb[1],tb[2]))
                    lastnum = tb[1]
        else:
            msg.append("   Function was never called")
        self.printwithprefix(msg, self.prefix)
    def printwithprefix(self, msg, prefix):
        for i in msg:
            print "%s %s"%(prefix,i)
    def printhistogram(self, bins=10, msize=40):
        hist = [0]*bins
        step = (self.hightime - self.lowtime)/bins
        for v in self.histtime:
            idx = int((v-self.lowtime)/step)
            #border case check for hightime
            if idx == bins:
                idx -= 1
            hist[ idx ] += 1
        msg = []
        msg.append("   HIST(bins: %d step: %f ms)"%(bins, step*1000))
        maxv = hist[0]
        for i in hist:
            if i > maxv:
                maxv = i
        for idx in xrange(len(hist)):
            i = hist[idx]
            full = float(i)/float(maxv)*msize
            rest = (full - int(full))
            c=''
            if 1/3. <= rest < 2/3.: c = '-'
            elif 2/3. <= rest < 1.: c = '+'
            gstring="#"*int(full)+c+" "*(msize-int(full)-len(c))
            mbv=int(math.log(maxv)/math.log(10.))+1
            mbv = ("%%%dd"%(mbv))%(i)
            msg.append("   % 11.5f ms %s|%s|"%((self.lowtime+step*idx)*1000, mbv, gstring ))
        self.printwithprefix(msg, self.prefix)
    def __call__(self, f):
        def wrapper(*args, **kwargs):
            timeit = time.time()
            ret = self.f(*args, **kwargs)
            timeit = time.time() - timeit

            if self.callcount == 0:
                self.callcount = 1
                self.lowtime = timeit+1
                self.hightime = timeit-1
                self.timesum = 0.
                self.histtime = []
            if timeit < self.lowtime:
                self.lowtime = timeit
                self.lowtimeargs = args
                self.lowtimekwargs = kwargs
            if timeit > self.hightime:
                self.hightime = timeit
                self.hightimetb = traceback.extract_stack()[0:-1]
                self.hightimeargs = args
                self.hightimekwargs = kwargs
            if self.printhist:
                self.histtime.append(timeit)
            self.timesum += timeit
            self.callcount += 1
            return ret
        self.f = f
        wrapper.__name__ = f.__name__
        wrapper.__dict__.update(f.__dict__)
        wrapper.__doc__ = f.__doc__
        return wrapper


if __name__ == "__main__":
    def __test():
        @Timeit(printtb=True,printhist=True, prefix="PREFIX")
        def testing(i, msg=None):
            time.sleep(i//100)
            if msg:
                print "Testprint:",msg
        
        for i in xrange(100):
            if i % 3 == 0:
                testing(i,msg="mod3")
            else:
                testing(i)
    __test()    
