#!/usr/bin/python
import math
import getopt
import sys
from copy import deepcopy

def signum( i):
    if i < 0: return -1
    if i > 0: return 1
    return 0

def discordant(pair1, pair2):
    s1 = signum(pair1[0] - pair2[0])
    s2 = signum(pair1[1] - pair2[1])
    if( -s1 == s2):
        return 1
    return 0

def concordant(pair1, pair2):
    s1 = signum(pair1[0] - pair2[0])
    s2 = signum(pair1[1] - pair2[1])
    if( s1 == s2): 
        return 1
    return 0

def insertrank(pairlist, elem):
    pairlist.sort(lambda x,y: signum(x[elem] - y[elem]))
    i=0
    while( i < len(pairlist)):
        e=1 
        while( i+e < len(pairlist) and pairlist[i][elem] == pairlist[i+e][elem]): 
            e += 1
        for j in xrange(i,i+e):
            pairlist[j][elem] = i+1 + (float(e)-1.)/2.  
        i += e

def splitpairs(input):
    ilist = input.split(',')
    if len(ilist) < 2:
        print input, " only one item found (use , as delimiter)"
        print ilist
        quit(1)
    if len(ilist) > 2:
        print input, "contains more than two elements, only the first two will be taken into account"
    try: 
        a = float(ilist[0])
    except ValueError:
        print "'",ilist[0],"' is no float, assuming 0.0"
        a = 0.
    try:
        b = float(ilist[1])
    except ValueError:
        print "'",ilist[1],"' is no float, assuming 0.0"
        b = 0.
    return [a,b]

def sroc(opairlist):
    pairlist = deepcopy(opairlist)
    insertrank(pairlist, 0)
    insertrank(pairlist, 1)
    n =float( len(pairlist));
    dsquare = sum( [(i[0]-i[1])**2 for i in pairlist] )
    if n > 0:
        sroc = 1. - 6*float(dsquare)/(n*(n**2-1)) 
    else:
        sroc = 0
    return sroc

def correlation(opairlist):
    pairlist = deepcopy(opairlist)
    n =float( len(pairlist))
    m_0 = sum([i[0] for i in pairlist])/n;
    m_1 = sum([i[1] for i in pairlist])/n;
    s_0 = math.sqrt( sum( [(i[0] - m_0)**2 for i in pairlist]  )/n)
    s_1 = math.sqrt( sum( [(i[1] - m_1)**2 for i in pairlist]  )/n)
    cov = sum( [(i[0] - m_0)*(i[1]-m_1) for i in pairlist]  )/n
    cor = cov/(s_0*s_1)
    return cor

def kendall(opairlist):
    pairlist = deepcopy(opairlist)
    x = [i[0] for i in pairlist]
    y = [i[1] for i in pairlist]
    S = 0
    n1 = 0
    n2 = 0
    for i in range(len(x)-1):
        for j in range(i+1,len(y)):
            s1 = x[i] - x[j]
            s2 = y[i] - y[j]
            sig = s1*s2
            if (sig):
                n1 += 1
                n2 += 1
                if sig > 0:
                    S += 1
                else:
                    S -= 1
            else:
                if s1:
                    n1 += 1
                if s2:
                    n2 += 1
    taub = S/math.sqrt(float(n1*n2))
    return taub

def opthandler():
    options = {}
    options["linear"] = False
    options["spearman"] = False
    options["kendall"] = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hlsk',['help', 'linear', 'spearman', 'kendall'] ) 
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit() 
        elif opt in ("-l", "--linear"):
            options["linear"] = True
        elif opt in ("-s", "--spearman"):
            options["spearman"] = True
        elif opt in ("-k", "--kendall"):
            options["kendall"] = True
        else:
            print "Unhandled option, should not happen"
            sys.exit(2)
    return options,args


def usage():
    print "Reads data from stdin and prints correlation."
    print "Input should consist of comma (',') delimited pairs with newline"
    print "as seperator for pairs, like so:"
    print "1,2"
    print "2,3"
    print "4.5,1e10"
    print ""
    print "    -h --help                      Prints this list."
    print "    -l --linear                    linear correlation"
    print "    -s --spearman                  spearman rank order correlation"
    print "    -k --kendall                   kendall tau rank order correlation"


def main():
    options, restargs = opthandler()

    if not (options['linear'] or options['kendall'] or options['spearman']):
        print "EE   Please give a correlation to output"
        usage()
        sys.exit(1)

    data = sys.stdin.read()
    data = data.splitlines()
    if len(data) <= 1:
        if len(data) == 0:
            usage()
            sys.exit(1)
        else:
            print "raking one pair is kind of pointless"
            quit(0)
    data = map(splitpairs, data)
    if options['linear']:
        usedata = data
        print "correlation =",correlation(usedata)
    if options['kendall']:
        usedata = data
        print "kendall =",kendall(usedata)
    if options['spearman']:
        usedata = data
        print "spearman =",sroc(usedata)
    x = [i[0] for i in data]
    y = [i[1] for i in data]

if __name__ == "__main__":
    main()

