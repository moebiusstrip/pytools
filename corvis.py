#!/usr/bin/python

import math
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

def genrect(x,y,width,height):
    return [x,y,x+width,y+height]

def genrectc(x,y,width,height):
    return genrect(int(x-width/2), int(y-height/2), width, height)

def lincmapbw(val):
    if val > 0:
        return (255,255,255)
    else:
        return (0,0,0)

def lincmap(val):
    rval = 1-abs(val)
    if val > 0:
        return (int(rval*255), int(rval*255), 255)
    else:
        return (255, int(rval*255), int(rval*255))

def lincmapgray(val):
    rval=0.5
    rval+=float(val)*0.5
    return (int(rval*255), int(rval*255), int(rval*255))

def nameplot(im, names, tilesize=75, background=(255,255,255),color=(0,0,0)):
    font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSans.ttf", tilesize/2)
    border=font.getsize("M")[0]

    xext=max([ font.getsize(i)[0] for i in names[0]])+2*border
    yext=max([ font.getsize(i)[0] for i in names[1]])+2*border
    
    namedim = Image.new("RGB", (im.size[0]+xext,im.size[1]+yext))
    draw = ImageDraw.Draw(namedim)
    draw.rectangle(((0,0),namedim.size), fill=background)
    namedim.paste(im,(xext-1,yext-1))

    for i in range(len(names[0])):
        ypos=yext + i*tilesize + (tilesize-font.getsize(names[0][i])[1])/2
        draw.text((border,ypos),names[0][i],font=font, fill=color)

    for i in range(len(names[1])):
        txt = Image.new("RGB",font.getsize(names[1][i]))
        txtdraw = ImageDraw.Draw(txt)
        txtdraw.rectangle(((0,0),txt.size), fill=background)
        txtdraw.text((0,0),names[1][i], font=font, fill=color)
        xpos=xext + i*tilesize + (tilesize-font.getsize(names[1][i])[1])/2
        namedim.paste(txt.rotate(90),(xpos,border))
    return namedim

def circleplot(data,names,cmap, scale=False, tilesize=75, background=(128,128,128), grid=(255,255,255)):
    if tilesize < 15:
        print "tilesize too small"
        exit(0)
    xtiles=len(data[0])
    ytiles=len(data)
    im = Image.new("RGB",(xtiles*tilesize+1, ytiles*tilesize+1))
    draw = ImageDraw.Draw(im)
    for i in xrange(xtiles):
        for j in xrange(ytiles):
            draw.rectangle(genrect(i*tilesize, j*tilesize, tilesize, tilesize),fill=grid)
            draw.rectangle(genrect(i*tilesize+1, j*tilesize+1, tilesize-2, tilesize-2),fill=background)
    #draw.rectangle(((0,0),(xtiles*tilesize, ytiles*tilesize)),fill=background)
    tilestep = tilesize/2
    for r in range(len(data)):
        for c in range(len(data[r])):
            x = c*tilesize + tilestep
            y = r*tilesize + tilestep
            aval = float(abs(data[r][c]))
            boxo=genrectc(x, y, tilesize-4, tilesize-4)
            box=genrectc(x,y,tilesize-10,tilesize-10)
            if scale:
                scalerange=tilesize-15
                scalefact=float(scalerange)/tilesize
                scalefix=float(tilesize-scalerange)/tilesize
                sval=aval*scalefact + scalefix
                boxo=genrectc(x+1,y+1, tilesize*sval-4, tilesize*sval-4)
                box=genrectc(x+1,y+1,tilesize*sval-10,tilesize*sval-10)
            draw.ellipse(boxo, fill=(0,0,0))
            draw.ellipse(box, fill=cmap(data[r][c]))
    im = nameplot(im, names, tilesize)
    return im

def shapecircle(val, cmap, tilesize=73, background=(128,128,128), border=True):
    im = Image.new("RGB",(tilesize,tilesize))
    draw = ImageDraw.Draw(im)
    frect = genrect(0,0,tilesize,tilesize)
    draw.rectangle(frect, fill=background)
    bordersize=0
    aval = float(abs(val))
    scalerange=tilesize-15
    scalefact=float(scalerange)/tilesize
    scalefix=float(tilesize-scalerange)/tilesize
    sval=aval*scalefact + scalefix
    csize=tilesize*sval
    center=(tilesize+1)/2
    if border:
        bordersize=4
        draw.ellipse(genrectc(center,center,csize-2,csize-2), fill=(0,0,0))
    draw.ellipse(genrectc(center,center,csize-2-bordersize,csize-2-bordersize), fill=cmap(val))
    return im

def shapepie(val, cmap, tilesize=73, background=(128,128,128), border=True):
    im = Image.new("RGB",(tilesize,tilesize))
    draw = ImageDraw.Draw(im)
    frect = genrect(0,0,tilesize,tilesize)
    draw.rectangle(frect, fill=background)
    bordersize=0
    center=(tilesize+1)/2
    if border:
        bordersize=4
        draw.ellipse(genrectc(center,center,tilesize-2,tilesize-2), fill=(0,0,0))
        draw.ellipse(genrectc(center,center,tilesize-2-bordersize,tilesize-2-bordersize), fill=background)
    if val < 0:
        draw.ellipse(genrectc(center,center,tilesize-2-bordersize,tilesize-2-bordersize), fill=cmap(val))
    start=0
    end=int(val*360)
    if abs(end) == 360:
        draw.ellipse(genrectc(center,center,tilesize-2-bordersize,tilesize-2-bordersize), fill=cmap(val))
    else:
        if val < 0:
            draw.pieslice(genrectc(center,center,tilesize-2-bordersize,tilesize-2-bordersize),start,end, fill=background, outline=(0,0,0))
        else:
            draw.pieslice(genrectc(center,center,tilesize-2-bordersize,tilesize-2-bordersize),start,end, fill=cmap(val), outline=(0,0,0))
    return im


def shapebars(val, cmap, tilesize=None, xsize=73, ysize=73, background=(128,128,128), border=True):
    if tilesize != None:
        xsize, ysize = tilesize, tilesize
    im = Image.new("RGB",(xsize,ysize))
    draw = ImageDraw.Draw(im)
    frect = genrect(0,0,xsize,ysize)
    draw.rectangle(frect, fill=background)
    aval = float(abs(val))
    dist = int(float(ysize-2)*aval)
    x,y,bordersize=1,1,0
    if val < 0:
        y=ysize-dist-1
    if border:
        bordersize=4
        draw.rectangle(genrect(x,y,xsize-2,dist), fill=(0,0,0))
    draw.rectangle(genrect(x+bordersize/2,y+bordersize/2,xsize-2-bordersize,dist-bordersize), fill=cmap(val))
    return im
    
def shapetext(val, cmap, tilesize=73, background=(128,128,128), border=True):
    im = Image.new("RGB",(tilesize,tilesize))
    draw = ImageDraw.Draw(im)
    frect = genrect(0,0,tilesize,tilesize)
    draw.rectangle(frect, fill=background)
    aval = float(abs(val))
    dist = int(float(tilesize-2)*aval)
    x=1
    y=1
    fontsize=tilesize/2
    size=tilesize+1
    while( size > tilesize ):
        fontsize -= 1
        font = ImageFont.truetype("/usr/share/fonts/truetype/freefont/FreeSans.ttf", fontsize)
        size = font.getsize("-100")[0]
    txt = "%d" %(int(val*100))
    size = font.getsize(txt)
    xpos = (tilesize-size[0]+2)/2
    ypos = (tilesize-size[1]+2)/2
    
    if border:
        draw.text((xpos-1,ypos-1), txt, font=font, fill=(0,0,0))
        draw.text((xpos+1,ypos-1), txt, font=font, fill=(0,0,0))
        draw.text((xpos-1,ypos+1), txt, font=font, fill=(0,0,0))
        draw.text((xpos+1,ypos+1), txt, font=font, fill=(0,0,0))
    draw.text((xpos,ypos), txt, font=font, fill=cmap(val))
    return im   


def plot(data,names,cmap, shape=shapecircle, tilesize=75, background=(128,128,128), grid=(255,255,255)):
    if tilesize < 15:
        print "tilesize too small"
        exit(0)
    xtiles=len(data[0])
    ytiles=len(data)
    im = Image.new("RGB",(xtiles*tilesize+1, ytiles*tilesize+1))
    draw = ImageDraw.Draw(im)
    for i in xrange(xtiles):
        for j in xrange(ytiles):
            draw.rectangle(genrect(i*tilesize, j*tilesize, tilesize, tilesize),fill=grid)
            draw.rectangle(genrect(i*tilesize+1, j*tilesize+1, tilesize-2, tilesize-2),fill=background)
    for j in xrange(ytiles):
        for i in xrange(xtiles):
            x = i*tilesize + 1
            y = j*tilesize + 1
            im.paste( shape(data[j][i], cmap, tilesize=tilesize-2, background=background), (x,y))   
    im = nameplot(im, names, tilesize)
    return im

def multishapebars(valarr, cmap, tilesize=73, background=(128,128,128), border=True):
    if len(valarr) == 1:
        return shapebars(valarr[0], cmap, tilesize=tilesize, background=background, border=border)
    im = Image.new("RGB",(tilesize,tilesize))
    draw = ImageDraw.Draw(im)
    frect = genrect(0,0,tilesize,tilesize)
    draw.rectangle(frect, fill=background)
    aval = float(abs(valarr[0]))
    dist = int(float(tilesize-2)*aval)
    x=1
    y=1
    if valarr[0] < 0:
        ybar=tilesize-dist-1
    else:
        ybar=dist+1
    items = len(valarr)-1
    width=(tilesize-2-(items-1))/items
    alignborder=(tilesize-2-(width+1)*items+1)/2
    for i in range(items):
        im.paste(shapebars(valarr[i+1], cmap, ysize=tilesize-2, xsize=width, background=background, border=border), (x+alignborder+(width+1)*i, y))
    if border:
        draw.rectangle((x, ybar-1, tilesize-1, ybar+1), fill=(0,0,0))
    draw.line((x+1,ybar,tilesize-2,ybar),fill=cmap(valarr[0]), width=1)
        
    
    return im
    

def arrange( images, tilesize=73, background=(128, 128, 128), layout="grid"):
    """ layout grid is fallback, layout spread is side by side, mainly for bars"""
    im = Image.new("RGB", (tilesize, tilesize))
    draw = ImageDraw.Draw(im)
    draw.rectangle(genrect(0,0,tilesize, tilesize),fill=background)
    numim = len(images)
    if numim == 0:
        return im
    if numim == 1:
        return images[0]

    xsize, ysize = tilesize-(numim-1), tilesize-(numim-1)
    if layout == "spread":
        xsize = (tilesize - (numim-1))/numim
        leftborder = (tilesize - (xsize+1)*numim + 1)/2
        for i in range(numim):
            pim = images[i].resize((xsize,ysize), Image.BILINEAR)
            im.paste(pim,(leftborder+i*(xsize+1),0))
    else: #layout grid and fallback
        linesize= int(math.sqrt(numim-1)+1)
        xsize = (tilesize - (linesize-1))/linesize
        ysize = xsize
        border = (tilesize - (xsize+1)*linesize +1)/2
        for i in range(numim):
            x = i % linesize
            y = i / linesize
            pim = images[i].resize((xsize,xsize), Image.BICUBIC)
            im.paste(pim, (border+x*(xsize+1), border+y*(xsize+1)))

    return im

def multiplot(dataarr,names,cmap, shape=shapecircle, multishape=None, tilesize=75, background=(128,128,128), grid=(255,255,255), layout="grid"):
    data = dataarr[0]
    if tilesize < 15:
        print "tilesize too small"
        exit(0)
    xtiles=len(data[0])
    ytiles=len(data)
    im = Image.new("RGB",(xtiles*tilesize+1, ytiles*tilesize+1))
    draw = ImageDraw.Draw(im)
    for i in xrange(xtiles):
        for j in xrange(ytiles):
            draw.rectangle(genrect(i*tilesize, j*tilesize, tilesize, tilesize),fill=grid)
            draw.rectangle(genrect(i*tilesize+1, j*tilesize+1, tilesize-2, tilesize-2),fill=background)
    for j in xrange(ytiles):
        for i in xrange(xtiles):
            x = i*tilesize + 1
            y = j*tilesize + 1
            if multishape == None:
                tmpim = []
                for data in dataarr:
                    tmpim.append( shape(data[j][i], cmap, tilesize=tilesize-2, background=background))
                im.paste( arrange(tmpim, tilesize=tilesize-2, background=background, layout=layout), (x,y)) 
            else:
                data = []
                for sdata in dataarr:
                    data.append(sdata[j][i])
                im.paste( multishape(data, cmap, background=background, tilesize=tilesize-2), (x,y))
    im = nameplot(im, names, tilesize)
    return im



def main():
    data=[
        [ 0.0,-0.2, 0.4],
        [-0.5, 0.6,-0.7],
        [ 0.8,-0.9, 1.0],
    ]
    names=[["row1","row2","row3"],["col1","col2","col3"]]
    im = circleplot(data, names, lincmap, scale=True)
    im.save("output.png","PNG")
    im = circleplot(data, names, lincmapgray, scale=True,background=(255,255,255), grid=(100,100,100), tilesize=100)
    im.save("outputg.png","PNG")
    data=[[-1.0,-0.85,-0.70,-0.55,-0.4,-0.25,-0.1,0,0.05,0.2,0.35,0.50,0.65,0.8,0.95]]
    names=[["icon"],["-100","-85","-70","-55","-40","-25","-10","0","5","20","35","50","65","80","95"]]
    im = plot(data,names, lincmap, shape=shapecircle, background=(255,255,255), grid=(128,128,128))
    im.save("plotnewcirle.png","PNG")
    im = plot(data,names, lincmap, shape=shapebars, background=(255,255,255), grid=(128,128,128))
    im.save("plotnewbars.png","PNG")
    im = plot(data,names, lincmap, shape=shapepie, background=(255,255,255), grid=(128,128,128))
    im.save("plotnewpie.png","PNG")
    im = plot(data,names, lincmap, shape=shapetext, background=(255,255,255), grid=(128,128,128))
    im.save("plotnewtext.png","PNG")
    data2=[[i+0.05 for i in data[0]]]
    data3=[[i*0.5 for i in data[0]]]
    dataarr=[data,data2,data3]
    im = multiplot(dataarr, names, lincmap, shape=shapebars, layout="spread", tilesize=100)
    im.save("multiplotbars.png","PNG")
    im = multiplot(dataarr, names, lincmap, shape=shapepie, layout="grid", tilesize=100)
    im.save("multiplotpie.png","PNG")
    im = multiplot(dataarr, names, lincmap, multishape=multishapebars, tilesize=100)
    im.save("multiplotmultishapebars.png","PNG")

if __name__ == "__main__":
    main()

