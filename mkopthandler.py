#!/usr/bin/python
import sys

def getoptname(element):
    if element[1] != None:
        return element[1]
    return element[0]

def createimports():
    print """
import getopt
import sys
"""

def createmain():
    print """
def main():
    options, restargs = opthandler()
#   print "options:"
#   print options
#   print "rest arguments:"
#   print restargs

if __name__ == "__main__":
    main()
"""

def createusage(args):
    arglist = "\ndef usage():\n"
    arglist += '    print "    -h --help                      Prints this list."\n';
    for arg in args:
        arglist += '    print "    -%s ' % arg[0]
        if arg[1] == None:
            arglist += "               ";
        else:
            arglist += "--%-13s" %arg[1]
        if arg[2]:
            arglist += " <option>"
        else:
            arglist += "         "
        arglist += '    <<< describe %s >>>"\n' % getoptname(arg)
    print arglist

#short, long, arg, default
def createhandler(args):
    args.sort(lambda a,b: a[1] == None and 1 or -1 )
    defaults = ""
    for arg in args:
        if( arg[3] != None):
            defaults +="    options[\"%s\"] = \"%s\"\n" % (getoptname(arg), arg[3])
        elif not arg[2]: ## this is a flag init with False
            defaults +="    options[\"%s\"] = False\n" % (getoptname(arg))
    short = "h"
    for arg in args:
        short += arg[0]
        if arg[2]:
            short += ":"
    long = ['help']
    for arg in args:
        if arg[1] != None:
            if arg[2]:
                long.append( arg[1]+"=")
            else:
                long.append(arg[1])
    print """
def opthandler():
    options = {}
%s
    try:
        opts, args = getopt.getopt(sys.argv[1:], '%s',%s ) 
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit() """ % (defaults, short, long)
#   do all the other stuff: 
    block = ""
    for arg in args:
        block = "       elif opt in (\"-%s\"" %arg[0]
        if arg[1] != None:
            block += ', "--%s"' % arg[1]
        block += "):\n"
        if arg[2]: # with parameter
            block += '          options["%s"] = arg' % getoptname(arg)
        else: # flag
            block += '          options["%s"] = True' % getoptname(arg)
        print block
    print """       else:
            print "Unhandled option, should not happen"
            sys.exit(2)"""
    print " return options,args"        

def checkduplicates(list):
    count = {}
    ret = False
    for elem in list:
        if elem != None:
            if elem in count:
                count[elem] += 1
            else:
                count[elem] = 1
    for key in count:
        if count[key] > 1:
            ret = True
            print key," appears more than once (",count[key],")"
        if key == "h" or key == "help":
            ret = True
            print "The -h, --help option is automatically added, please do not use either h nor help"
    return ret

def extractargs(element):
    short = long = default = None
    reqarg = False
    split = element.split('=')
    if len(split) == 2:
        reqarg = True;
        if len(split[1]) > 0:
            default = split[1]
    split = split[0].split(',');
    if len(split) == 2:
        long = split[1]
    short = split[0]

    if len(short) == 0:
        print "short has length 0 in ",element
        sys.exit(2)
    if long != None and len(long) == 0:
        print "long has length 0 in ",element, " clearing long argument"
        long = None
        sys.exit(1)
    return (short, long, reqarg, default)

if len(sys.argv) < 2:
    print "give parameter list as: short,long[=[default value]]:..."
    sys.exit(1)

data = sys.argv[1].split(":")
args = [ extractargs(i) for i in data]
if checkduplicates([a[0] for a in args]) or checkduplicates([a[1] for a in args]):
    sys.exit(2)

createimports()
createhandler(args) 
createusage(args)
createmain()
#print args
