import os
import tempfile
import mytools

def subst(s, **kwargs):
    ret = ""
    try:
        ret = s.format(**kwargs)
    except KeyError as e:
        nv = str(e)+"-UNKNOWN"
        print "KeyError:" +str(e)+" needs a definition, set to "+nv
        nargs=dict(kwargs)
        nargs[str(e)[1:-1]]=nv
        ret = subst(s, **nargs)
    return ret

def _writeplotfile( string=None):
    path = None
    if string:
        (o, path) = tempfile.mkstemp()
        o = os.fdopen(o,'w')
        o.write(string)
        o.close()
    return path

def genplot_string(string="", **kwargs):
    pf=""
    for l in string.split("\n"):
        pf += subst(l+"\n", **kwargs)
    return _writeplotfile(pf)

def genplot(template="gnuplot.template",**kwargs):
    t = open(template,'r')
    pf =""
    for l in t:
        pf += subst(l, **kwargs)
    t.close()
    return _writeplotfile(pf)

def gnuplot(fname, delafterplot=True):
    ret = mytools.callext('gnuplot '+fname)
    if delafterplot:
        os.unlink(fname)
    return ret

def plot(template="gnuplot.tempalte", **kwargs):
    gnuplot( genplot( template, **kwargs), delafterplot=True)

def plot_string(string="", **kwargs):
    gnuplot( genplot_string( string, **kwargs), delafterplot=True)
