#!/usr/bin/python
import os
import subprocess

def flattengen(lst):
    for elem in lst:
        if type(elem) in (tuple, list):
            for i in flatten(elem):
                yield i
        else:
            yield elem

def flatten(lst):
    return list(flattengen(lst))

def callext(command):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, shell=True)
    ret=[]
    while True:
        for line in p.stdout:
            ret.append(line.rstrip("\n"))
        if( p.poll() != None):
            break
    if( p.poll() != 0):
        print "Exit status for "+command+" was "+str(p.poll())+": breaking"
        exit(1)
    return ret

def properpath(pathstring):
    if pathstring.find("/") > 0:
        spl = pathstring.split("/")
    elif pathstring.find("\\") > 0:
        spl = pathstring.split("\\")
    else:
        spl = [pathstring]
    return reduce(os.path.join, spl)

def print_data(data, space=0, spacer="  "):
    line=spacer*space
    if isinstance(data, dict):
        for k,v in data.items():
            print line+str(k)
            print_data(v, space+1, spacer)
    elif isinstance(data, list):
        for l in data:
            print_data(l, space+1, spacer)
    else:
        print line+str(data)

def main():
    a=[["a",1],2,[[3],["b",4],5],6]
    print_data(a)
    print a," => ", list(flatten(a))
    print callext("ls")
    wp="c:\\porn\\tities.png"
    up="/private/porn/tities.png"
    print up," => ", properpath(up)
    print wp," => ", properpath(wp)
    

if __name__ == "__main__":
    main()
