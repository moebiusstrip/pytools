import ConfigParser

class parsecfg(ConfigParser.SafeConfigParser):
    """Acts as ConfigParser.SafeConfigParser but is able to read sectionless config files.
    If a config file is sectionless it will be added to [DEFAULT]"""
    def read(self, cfglist):
        if isinstance(cfglist, (list,tuple)):
            for f in cfglist:
                self._safe_read(cfglist)
        elif isinstance( cfglist, str):
            self._safe_read(cfglist)
        else:
            raise TypeError('expected type list,tuple or string: got type',type(cfglist))
    def _safe_read(self, fname):
        try:
            ConfigParser.SafeConfigParser.read(self, fname)
        except ConfigParser.MissingSectionHeaderError:
            self._safe_read_noheader(fname)
    def _safe_read_noheader(self, fname):
        class fakeheader:
            def __init__(self, fname, sectionname=None):
                self.fp = open(fname)
                self.inserthead = '[DEFAULT]'
                if sectionname:
                    self.inserthead = sectionname
            def readline(self):
                if self.inserthead:
                    try:
                        return self.inserthead
                    finally:
                        self.inserthead = None
                else:
                    return self.fp.readline()
        ConfigParser.SafeConfigParser.readfp(self, fakeheader(fname))


