import sys
import imp
import os
import os.path
import inspect
try:
    import xdg.BaseDirectory as xdg_bd
except ImportError:
    class xdg_bd:
        xdg_data_home="/tmp/"
    if "xdg_data_home" in os.environ:
        xdg_bd.xdg_data_home = os.environ["xdg_data_home"]
    elif "HOME" in os.environ:
        xdg_bd.xdg_data_home = os.path.join( os.environ["HOME"], ".local", "share")

class pluginloader:
    """ loads plugins from xdg_data_home/basename/plugins
        Plugins are files ending in .py (single python files).
        The have to contain a class ending in Plugin and otherwise 
        have the same basename as the file: testPlugin in test.py
    """
    def __init__(self, basename=None):
        self.basename=basename
        if self.basename is None:
            self.basename = os.path.basename(sys.argv[0]).rstrip(".py")
        self.plugindir = os.path.join(xdg_bd.xdg_data_home, self.basename, 'plugins')
        self.plugins = {}
        self.__scandirectory()
    def __scandirectory(self):
        if not os.path.isdir(self.plugindir):
            return
        for i in os.listdir( self.plugindir):
            if not i.endswith(".py"):
                continue
            i = i.rstrip(".py")
            loadinfo = imp.find_module(i, [self.plugindir])
            self.plugins[i] = loadinfo
    def __repr__(self):
        ret = "pluginloader:\n"
        ret += "  .basename = "+str(self.basename)+"\n"
        ret += "  .plugindir = "+str(self.plugindir)+"\n"
        ret += "  PLUGINS: "+("" if os.path.isdir(self.plugindir) else "plugin directory does not exist")+"\n"
        for i in self.plugins.keys():
            ret+="    "+i+": "+str(self.plugins[i])+"\n"
        return ret
    def load(self, name, *args, **kwargs):
        """ load module with name and give args for a call to __init__"""
        ret = None
        if name in self.plugins:
            tmp = imp.load_module(name, *self.plugins[name])
            if callable(getattr(tmp, name+"Plugin")):
                ret = getattr(tmp, name+"Plugin")(*args, **kwargs)
        return ret
    def listPluginConfig(self):
        """ load each module and show the name and arguments allowed, use this for a usage function 
            If an option starts with _ it is assumed to be a base option added by the program, i.e. not shown to the user
        """
        for name in self.plugins:
            tmp = imp.load_module(name, *self.plugins[name])
            if callable(getattr(tmp, name+"Plugin")):
                obj = getattr(tmp, name+"Plugin")
                optlist=[]
                if hasattr( obj, "__init__"):
                    fun = getattr(obj, "__init__")
                    arglist = inspect.getargspec(fun)
                    arglist[0].remove("self")
                    args = arglist[0]
                    defaultslist = []
                    if not arglist[3] is None:
                        defaultslist = [ d for d in arglist[3] ]
                    while len(defaultslist) < len(args):
                        defaultslist.insert(0, None)
                    for idx in range(len(args)):
                        if not args[idx].startswith("_"):
                            if defaultslist[idx] is None:
                                optlist.append(args[idx])
                            else:
                                optlist.append(args[idx]+"="+defaultslist[idx])

                    optlist=", ".join(optlist)
                if len(optlist)>0:
                    print "\t{}: {}".format(name, optlist)
                else:
                    print "\t{}: No options".format( name)


if __name__ == '__main__':
    pl = pluginloader(basename="testbasename")
    print pl
    pl = pluginloader()
    print pl
    cwm = pl.load("test",cmsg="class message")
    cnm = pl.load("test")
    cnm.run("asdf")
    cwm.run("asdf", optmsg="and optional")
