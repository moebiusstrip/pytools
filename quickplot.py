#!/usr/bin/env python
# vim: set fileencoding=utf-8
__author__ ="Heinz Hofbauer" 
__email__  ="hofbauer.heinz@gmx.net"
__source__ ="https://bitbucket.org/moebiusstrip/pytools"

import tempfile
import os
import getopt
import sys
import re
import myplot
import mytools
import numpy

def exitwitherror(msg):
    print("Error: %s"%(msg))
    sys.exit(3)

def selectoutlier( val, avg, std, highlow=None, distance=3):
    z = (float(val) - float(avg))/float(std)
    if abs(z) < distance:
        return False
    if highlow == None:
        return True
    elif highlow == "upper":
        if z > distance:
            return True
    elif highlow == "lower":
        if z < -distance:
            return True
    #no clue what the highlow options is supposed to be
    return False

def findrange(minval, maxval):
    diff = maxval-minval
    step=1.
    if diff < 1:
        while step / 10. > diff:
            step /= 10.
    else:
        while diff > step:
            step*=10
    lower=int(minval/step)*step
    upper=lower+step
    while maxval > upper:
        upper+=step
    return "[%f:%f]"%(lower, upper)


class Point:
    idx = 0
    def __init__(self, line, options):
        self.autox = False
        split = options['delimiter'].split(line)
        if max(options['fields']) >= len(split):
            exitwitherror("no field %d in line: %s"%(max(options['fields']), line))
        if options['fields'][0] == -1:
            self.label = str(Point.idx)
        else:
            self.label = split[options['fields'][0]]
            if options['label']:
                f = options['label'].findall(self.label)
                if len(f) > 0:
                    self.label = f[0]
                else:
                    exitwitherror("Could not squash %s with %s"%(self.label, options['label'].pattern))
        if options['fields'][1] == -1:
            self.x = Point.idx
            self.autox=True
        else:
            try:
                self.x = float(split[options['fields'][1]])
            except ValueError:
                exitwitherror("field %d (%s) is not a float"%(options['fields'][1], split[options['fields'][1]]))
        try:
            self.y = float(split[options['fields'][2]])
        except ValueError:
            exitwitherror("field %d (%s) is not a float"%(options['fields'][2], split[options['fields'][2]]))
        Point.idx+=1
    def __repr__(self):
        return "%s\t%f\t%f"%(self.label, self.x, self.y)
    def renumber(self, num):
        if self.autox:
            self.x = num
    def write(self, fh):
        fh.write("%f\t%f\n"%(self.x, self.y))

class Group:
    pattern = None
    idx = 0
    def __init__(self, p):
        if Group.pattern:
            self.x = Group.idx
        else:
            self.x = p.x
        self.minpoint = p
        self.maxpoint = p
        self.points =[p]
        self.avg = p.y
        self.std = 0
        self.label = self._squash(p)
        Group.idx+=1
    def renumber(self, num):
        self.x = num
        if Group.pattern == None:
            self.label = str(self.x)
    def match(self, p):
        plabel = self._squash(p, silent=True)
        return self.label == plabel
    def add(self, p):
        if not self.match( p):
            return False
        self.points.append(p)
        if p.y > self.maxpoint.y:
            self.maxpoint = p
        if p.y < self.minpoint.y:
            self.minpoint = p
        self.avg = numpy.mean( [ e.y for e in self.points])
        self.std = numpy.std( [ e.y for e in self.points])
        return True
    def _squash(self, p, silent=False):
        rlabel = ""
        if Group.pattern:
            #squash the label
            rlabel = "".join( [ fragment.strip() for fragment in mytools.flatten(Group.pattern.findall(p.label)) ])
            if rlabel == "" and not silent:
                print("warning empty label group for %s with pattern %s"%(p.label, Group.pattern.pattern))
        else:
            rlabel = str(p.x)
        return rlabel
    def __repr__(self):
        ret = "Group %s (%d points)\n"%(self.label, len(self.points))
        ret+= "\tmin: %s\n"%(str(self.minpoint))
        ret+= "\tmax: %s\n"%(str(self.maxpoint))
        ret+= "\tx, µ±σ: %f, %f±%f\n"%(self.x, self.avg,self.std)
        return ret
    def write(self, fh):
        fh.write("%f\t%f\t%f\t%f\t%f\t%f\n"%(
            self.x,   
            self.avg-self.std,   
            self.minpoint.y, 
            self.maxpoint.y, 
            self.avg+self.std,
            self.avg))

def teststrto(val, fun):
    ret = True
    try:
        sval = fun(val)
        if not str(sval) == val:
            ret = False
    except:
        ret = False
    return ret

class PlotData:
    def __init__(self, options):
        self.options = options
        self.datalist = []
        self._readinput()
        if options['group']:
            self.grouplist = []
            self._grouppoints()
        self.cleanuplist = []
        self.outlierlog={}
    def _grouppoints(self):
        Group.pattern = self.options['groupregex']
        for p in self.datalist:
            added=False
            for g in self.grouplist:
                added = g.add(p)
                if added:
                    break
            if not added:
                self.grouplist.append(Group(p))
        if Group.pattern:
            usefloat = True
            useint = True
            for g in self.grouplist:
                useint = useint and teststrto(g.label, int)
                usefloat = useint and teststrto(g.label, float)
            if useint:
                self.grouplist.sort( key=lambda g:int(g.label))
            elif usefloat:
                self.grouplist.sort( key=lambda g:float(g.label))
            else:
                self.grouplist.sort( key=lambda g:g.label)
            for i in xrange(len(self.grouplist)):
                self.grouplist[i].renumber(i+1)
    def _readinput(self):
        fh = None
        if self.options['input']:
            fh = open( self.options['input'], "r")
        else:
            fh = sys.stdin
            if sys.stdin.isatty():
                print("No file given, accepting input on stdin now")

        for line in fh:
            if line.startswith('#'):
                self._argument(line[1:])
            else:
                self.datalist.append( Point(line, self.options))
        if self.options['input']:
            fh.close()
        if self.options['fields'][1] == -1 and self.options['fields'][0] != -1: #autogen x numbers, redo if labesl are given
            usefloat = True
            useint = True
            for p in self.datalist:
                useint = useint and teststrto(p.label, int)
                usefloat = useint and teststrto(p.label, float)
            if useint:
                self.datalist.sort( key=lambda p:int(p.label))
            elif usefloat:
                self.datalist.sort( key=lambda p:float(p.label))
            else:
                self.datalist.sort( key=lambda p:p.label)
            for i in xrange(len(self.datalist)):
                self.datalist[i].renumber(i+1)
    def _argument(self, line):
        split = line.split(":")
        if len(split) < 2:
            return
        arg = split[0].strip()
        txt = ":".join(split[1:])
        if arg in ('title','xlabel','ylabel'):
            if self.options[arg] == None:
                self.options[arg] = txt.strip()
    def __repr__(self):
        ret = ""
        if self.options['group']:
            for g in self.grouplist:
                ret += str(g)+"\n"
        else:
            for p in self.datalist:
                ret += str(p)+"\n"
        return ret
    def _writedatafile(self, data):
        (o, datapath) = tempfile.mkstemp()
        o = os.fdopen(o,'w')
        #write plotdata
        for p in data:
            p.write(o)
        o.close()
        self.cleanuplist.append(datapath)
        return datapath
    def plot(self):
        if self.options['group']:
            plotdata = self._writedatafile(self.grouplist)
            plottemplate, plotargs = self._groupplot(plotinput=plotdata)
        else: #simple plot
            plotdata = self._writedatafile( self.datalist)
            plottemplate, plotargs = self._simpleplot(plotinput=plotdata)
        #do actual plot
        myplot.plot_string( plottemplate, **plotargs)

        #cleanup
        for f in self.cleanuplist:
            os.unlink(f)
        self.cleanuplist = []

    def _addoutlierlist(self, name, plist):
        if name not in self.outlierlog:
            self.outlierlog[name]=[]
        else:
            print("outlier list with name %s already exists, will skip this attempt"%(name))
            return
        for p  in plist:
            self.outlierlog[name].append(p)
    def _groupplot(self, **args):
        ylist = [ g.avg for g in self.grouplist]
        avg = numpy.mean(ylist)
        std = numpy.std(ylist)
        ymin = min(ylist)
        ymax = max(ylist)


        #check max lael length for canvas size:
        ll = [len(g.label) for g in self.grouplist]
        ll = max(ll)
        canvassize=""
        if ll > 10:
            if self.options['ext'] == "pdf":
                canvassize=" size 5in,4.5in" # up from 5x3
            elif self.options['ext'] == "png":
                canvassize=" size 640,580" # up from 640x480

        template = 'set terminal {filetype}cairo dashed font "Verdana"'
        template += canvassize  + '\n'
        args['filetype'] = self.options['ext']
        if self.options['key']:
            template += 'set key bmargin center horizontal\n'
        else:
            template += 'set key off\n'
        template += 'set output "{basename}.{filetype}"\n'
        template += 'set yrange %s\n'%(findrange(ymin,ymax))
        args['basename'] = self.options['output']
        for k in ['title', 'xlabel', 'ylabel']:
            if self.options[k]:
                template += 'set %s "%s"\n'%(k, self.options[k])
        maxtics = 20.
        mod = int(len(self.grouplist)/(maxtics-1))
        tlist = ""
        for idx in xrange(len(self.grouplist)):
            g = self.grouplist[idx]
            if idx == 0:
                tlist +="("
            else:
                tlist +=", "
            if ( (idx % mod == 0) and not ( float(mod)/(len(self.grouplist)-idx) > 2. ) ) or ( len(self.grouplist)-1 == idx ): 
                #major if mod tics or last
                tlist += '"%s" %f'%(g.label, g.x)
            else:
                #minor
                tlist += '"" %f 1'%(g.x)
        tlist += ")"
        if self.options['fields'][0] != -1:
            template += "set xtics rotate by -90 %s\n"%(tlist)
        else:
            template += "set xtics %s\n"%(tlist)

        #template+="set arrow from graph 0, first %f to graph 1, first %f nohead front\n"%(avg,avg) #TODO formating
        plotline = 'plot "{plotinput}" using 1:3:4  with filledcu title "[min(y),max(y)]" lc rgb "gray"'
        plotline+= ', "" using 1:6 with lines title "average" lt -1 lw %d lc rgb "black"'%( 1 if len(self.grouplist)>250 else 2)
        #upper maxpoint outlier:
        outlist = [ g for g in self.grouplist if selectoutlier(g.maxpoint.y, avg, std, "upper", distance=self.options['zscore'])]
        if self.options['printoutlier'] and len(outlist) > 0:
            self._addoutlierlist("High outlier per group", [ p.maxpoint.label for p in outlist])
        if len(outlist) > 0:
            outpath = self._writedatafile(outlist)
            plotline += ', "%s" using 1:4 with points pt 8 ps %3.2f lc rgb "dark-green" title "outlier (high)"'%(outpath, self.options['labelpointsize'])
            #labels
            outlist.sort(key=lambda g: g.maxpoint.y, reverse=True)
            outlist = outlist[:7]
            for g in outlist:
                template += 'set label "%s" at %f, %f front tc rgb "dark-green" font "Verdana,%d" offset character 0.5,0\n'%(g.maxpoint.label, g.x, g.maxpoint.y, self.options['labelfontsize'])
        #lower minpoint outlier:
        outlist = [ g for g in self.grouplist if selectoutlier(g.minpoint.y, avg, std, "lower", distance=self.options['zscore'])]
        if self.options['printoutlier'] and len(outlist) > 0:
            self._addoutlierlist("Low outlier per group", [ p.minpoint.label for p in outlist])
        if len(outlist) > 0:
            outpath = self._writedatafile(outlist)
            plotline += ', "%s" using 1:3 with points pt 10 ps %3.2f lc rgb "dark-red" title "outlier (low)"'%(outpath, self.options['labelpointsize'])
            outlist.sort(key=lambda g: g.minpoint.y )
            outlist = outlist[:7]
            for g in outlist:
                template += 'set label "%s" at %f, %f front tc rgb "dark-red" font "Verdana,%d" offset character 0.5,0\n'%(g.minpoint.label, g.x, g.minpoint.y, self.options['labelfontsize'])
        #average outlier
        outlist = [ g for g in self.grouplist if selectoutlier(g.avg, avg, std, distance=self.options['zscore'])]
        if self.options['printoutlier'] and len(outlist) > 0:
            self._addoutlierlist("Group average outlier", [ p.label for p in outlist])
        if len(outlist) > 0:
            outpath = self._writedatafile(outlist)
            plotline += ', "%s" using 1:6 with points pt 19 ps %3.2f lc rgb "red" title "group average outlier"'%(outpath, self.options['labelpointsize'])
            #labels
            outlist.sort(key=lambda g: g.maxpoint.y, reverse=True)
            outlist = outlist[:7]
            for g in outlist:
                template += 'set label "%s" at %f, %f front tc rgb "red" font "Verdana,%d" offset character 0.5,0\n'%(g.label, g.x, g.avg, self.options['labelfontsize'])

        #join template and plotlines:
        template += plotline+"\n"
        return template, args
    def _simpleplot(self, **args):
        ylist = [ p.y for p in self.datalist]
        avg = numpy.mean(ylist)
        std = numpy.std(ylist)
        ymin = min(ylist)
        ymax = max(ylist)

        #check max lael length for canvas size:
        ll = [len(p.label) for p in self.datalist]
        ll = max(ll)
        canvassize=""
        if ll > 10:
            if self.options['ext'] == "pdf":
                canvassize=" size 5in,4.5in" # up from 5x3
            elif self.options['ext'] == "png":
                canvassize=" size 640,580" # up from 640x480
        template = 'set terminal {filetype}cairo dashed font "Verdana"'
        template += canvassize + "\n"
        args['filetype'] = self.options['ext']
        if self.options['key']:
            template += 'set key bmargin center horizontal\n'
        else:
            template += 'set key off\n'
        template += 'set output "{basename}.{filetype}"\n'
        template += 'set yrange %s\n'%(findrange(ymin,ymax))
        args['basename'] = self.options['output']
        for k in ['title', 'xlabel', 'ylabel']:
            if self.options[k]:
                template += 'set %s "%s"\n'%(k, self.options[k])
        maxtics = 20.
        mod = int(len(self.datalist)/(maxtics)+1)
        tlist = ""
        for idx in xrange(len(self.datalist)):
            p = self.datalist[idx]
            if idx == 0:
                tlist +="("
            else:
                tlist +=", "
            if idx % mod == 0 or len(self.datalist)-1 == idx: 
                #major if mod tics or last
                tlist += '"%s" %f'%(p.label, p.x)
            else:
                #minor
                tlist += '"" %f 1'%(p.x)
        tlist += ")"
        if self.options['fields'][0] != -1:
            template += "set xtics rotate by -90 %s\n"%(tlist)
        else:
            template += "set xtics %s\n"%(tlist)

        #template+="set arrow from graph 0, first %f to graph 1, first %f nohead front\n"%(avg,avg) #TODO formating
        plotline = 'plot "{plotinput}" using 1:2 with lines lw %d lt -1 lc rgb "black" t "data"'%(1 if len(self.datalist)>500 else 2)
        plotline += ', %f with lines title "average" lw 2 lt 3 lc rgb "gray"'%(avg)
        #upper outlier:
        outlist = [ p for p in self.datalist if selectoutlier(p.y, avg, std, "upper", distance=self.options['zscore'])]
        if self.options['printoutlier'] and len(outlist) > 0:
            self._addoutlierlist("High point outlier", [ p.label for p in outlist])
        if len(outlist) > 0:
            outpath = self._writedatafile(outlist)
            plotline += ', "%s" using 1:2 with points pt 8 ps %3.2f lc rgb "dark-green" title "outlier (high)"'%(outpath, self.options['labelpointsize'])
            #labels
            outlist.sort(key=lambda p: p.y, reverse=True)
            outlist = outlist[:5]
            for p in outlist:
                template += 'set label "%s" at %f, %f front tc rgb "dark-green" font "Verdana,%d" offset character 0.5,0\n'%(p.label, p.x, p.y, self.options['labelfontsize'])

        #lower outlier:
        outlist = [ p for p in self.datalist if selectoutlier(p.y, avg, std, "lower", distance=self.options['zscore'])]
        if self.options['printoutlier'] and len(outlist) > 0:
            self._addoutlierlist("Low point outlier", [ p.label for p in outlist])
        if len(outlist) > 0:
            outpath = self._writedatafile(outlist)
            plotline += ', "%s" using 1:2 with points pt 10 ps %3.2f lc rgb "dark-red" title "outlier (low)"'%(outpath, self.options['labelpointsize'])
            outlist.sort(key=lambda p: p.y )
            outlist = outlist[:5]
            for p in outlist:
                template += 'set label "%s" at %f, %f front tc rgb "dark-red" font "Verdana,%d" offset character 0.5,0\n'%(p.label, p.x, p.y, self.options['labelfontsize'])
        template += plotline+"\n"
        return template, args


def convertoptions( options, reflags=re.UNICODE):
    for k in ('label','delimiter','groupregex'):
        if options[k] and isinstance(options[k], str):
            options[k] = re.compile(options[k], flags=reflags)
    if isinstance( options['fields'], str):
        options["fields"] = [ int(f) for f in options['fields'].split(":")]
        if len(options["fields"]) < 1:
            exitwitherror("fields should contain at least a single index")
        while len(options['fields']) < 3:
            options["fields"].insert(0,-1)
    if options['ext'] not in ["png","pdf"]:
        exitwitherror("%s is not a supported output format, use png or pdf"%(options['ext']))
    for k in ('group','key', 'printoutlier'):
        if isinstance( options[k], str):
            if options[k] in ("True","true","T","t","1"):
                options[k]=True
            else:
                options[k]=False
    for k in ('zscore',):
        if options[k] and isinstance( options[k], str):
            options[k] = float(options[k])
    if options['ext'] ==  "pdf": #adjust font for pdfs
        options['labelfontsize']=4
        options['labelpointsize'] = 1

def readconfig(options, filename):
    fh = open(filename, "r")
    fc = []
    for line in fh:
        if line.startswith('#'):
            continue
        fc.append(line.strip())
    for k in options.keys():
        for line in fc:
            if line.startswith(str(k)+"="):
                val = line[len(k)+1:]
                options[k]=val
    fh.close()

def opthandler():
    options = {}
    options["ext"] = "png"
    options["title"] = None
    options["ylabel"] = None
    options["xlabel"] = None
    options["input"] = None
    options["output"] = "quickplot"
    options["fields"] = "0:1:2"
    options["group"] = False
    options["groupregex"] = None
    options["delimiter"] = r"\s*"
    options["label"] = r".*"
    options["zscore"]=3
    options['labelfontsize']=6
    options['labelpointsize']=1.25
    options['key'] = True
    options['printoutlier'] = False
    options['showoptions'] = False

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'he:t:y:x:i:o:f:gd:l:c:z:',['help', 'ext=', 'title=', 'ylabel=', 'xlabel=', 'input=', 'output=', 'fields=', 'groupregex=', 'delimiter=', 'label=', 'config=', 'zscore=', 'nokey', 'printoutlier', 'showoptions'] ) 
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt,arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit() 
        elif opt in ("-e", "--ext"):
            options["ext"] = arg
        elif opt in ("-t", "--title"):
            options["title"] = arg
        elif opt in ("-y", "--ylabel"):
            options["ylabel"] = arg
        elif opt in ("-x", "--xlabel"):
            options["xlabel"] = arg
        elif opt in ("-i", "--input"):
            options["input"] = arg
        elif opt in ("-o", "--output"):
            options["output"] = arg
        elif opt in ("-f", "--fields"):
            options["fields"] = arg
        elif opt in ("-g"):
            options["group"] = True
        elif opt in ("--groupregex"):
            options["groupregex"] = arg
            options["group"] = True
        elif opt in ("-d", "--delimiter"):
            options["delimiter"] = arg
        elif opt in ("-l", "--label"):
            options["label"] = arg
        elif opt in ("-c", "--config"):
            readconfig(options, arg)
        elif opt in ("-z", "--zscore"):
            options["zscore"] = arg
        elif opt in ("--nokey"):
            options["key"] = False
        elif opt in ("--printoutlier"):
            options["printoutlier"] = True
        elif opt in ("--showoptions"):
            options["showoptions"] = True
        else:
            print "Unhandled option, should not happen"
            sys.exit(2)

    convertoptions(options)

    return options,args

def usage():
    print("Usage: "+str(sys.argv[0])+" [options]")
    print("    -h --help                            Prints this list.")
    print("    -e --ext           <png,pdf>         extension")
    print("    -t --title         <str>             title of the plot")
    print("    -y --ylabel        <str>             label for y-axis")
    print("    -x --xlabel        <str>             label for the x-axis")
    print("    -i --input         <file>            input file, if not given stdin is used")
    print("    -o --output        <str>             basename of output (sans extension)")
    print("    -f --fields        <[int]:[int]:int> index of fields to use for [label]:x:y")
    print("                                         label and x can be -1 (default), ")
    print("                                         they will be filled with a running number")
    print("    -l --label         <regex>           squash label to first group in regex")
    print("    -g                                   use x values for grouping ")
    print("       --groupregex    <regex>           use the regular expression on the labels for grouping")
    print("    -d --delimiter     <regex>           regular expresion to separate fields in the input file")
    print("    -c --config        <file>            read the config from a file")
    print("    -z --zscore        <int>             zscore for outlier detection, default: 3")
    print("       --nokey                           do not print the key of the figure")
    print("       --printoutlier                    print a list of the outliers")
    print("       --showoptions                     print a list of the options used for this execution")
    print("title, xlabel and ylabel can also be set from comment lines in the input")
    print("  example: #title: <the actual title>")
    print("NB: regex stuff is python regex, in case of squash if you want to use characters which ")
    print("    should not appear in the squash use a group.")
    print("")

def showoptions(options):
    print("#Options:")
    compre = re.compile("")
    for k in sorted(options.keys()):
        if options[k]:
            val = options[k]
            if type(val) == tuple or type(val) == list:
                val=":".join([str(v) for v in val])
            elif type(val) == type(compre):
                val = val.pattern
            print("%s=%s"%(k,val))

def main():
    options, restargs = opthandler()
    pd = PlotData(options)
    #print pd
    pd.plot()
    if options['printoutlier']:
        for k,l in pd.outlierlog.iteritems():
            print("")
            print("### "+k+ " ("+str(len(l))+")")
            for line in l:
                print(" * %s"%(line))
    if options['showoptions']:
        showoptions(options)

if __name__ == "__main__":
    main()

