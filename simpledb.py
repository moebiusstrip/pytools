#!/usr/bin/python
import MySQLdb
import mytools
import sqlite3

class coredb:
    db = None
    cursor = None

    def __del__(self):
        self.close()

    def close( self):
        if self.cursor != None:
            self.cursor.close()
            self.cursor = None
        if self.db != None:
            self.db.close()
            self.db = None

    def commit( self):
        if self.db != None:
            self.db.commit()

    def execute( self, command):
        if self.cursor == None: 
            return list()
        self.cursor.execute(command)
        result = self.cursor.fetchall()
        result = [[j for j in i] for i in result]
        return result

    def executeflat( self, command):
        if self.cursor == None: 
            return list()
        result = self.execute(command)
        result = list(mytools.flatten(result))
        return result
    
    def executeassoc(self, command):
        result = self.execute(command)
        for i in range(len(result)):
            cont = {}
            for j in range(len(result[i])):
                cont[self.cursor.description[j][0]] = result[i][j]
            result[i] = cont
        return result
                



class mysql(coredb):
    def __init__( self, user, db, **kwargs):
        self.open( user, db, **kwargs)
    def open( self, user, db, host="localhost", passwd=""):
        if self.db != None:
            close()
        self.db = MySQLdb.connect(host=host, passwd=passwd, user=user, db=db)
        self.cursor = self.db.cursor()

class sqlite(coredb):
    def __init__(self, fname):
        self.open(fname)
    def open(self, fname):
        self.db = sqlite3.connect(fname)
        self.cursor = self.db.cursor()

def main():
    pass

if __name__ == "__main__":
    main()
